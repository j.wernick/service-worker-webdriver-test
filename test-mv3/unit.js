/*
 * This file is part of eyeo's Web Extension Ad Blocking Toolkit (EWE),
 * Copyright (C) 2006-present eyeo GmbH
 *
 * EWE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * EWE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EWE.  If not, see <http://www.gnu.org/licenses/>.
 */

mocha.setup("bdd");
mocha.timeout(5000);

let messages = [];
let emit = () => {};

function sendMessage(id, arg) {
  messages.push([id, arg]);
  emit();
}

async function suspendServiceWorker() {
  sendMessage("suspendServiceWorker");
  await new Promise(r => setTimeout(r, 1000));
}


describe("Service Worker", function() {
  it("state is cleared when service worker is suspended", async function() {
    let sentMessage = "This is a message";
    await chrome.runtime.sendMessage({type: "set-message", message: sentMessage});
    let receivedMessageBefore = await chrome.runtime.sendMessage({type: "get-message"});
    chai.assert.equal(receivedMessageBefore, sentMessage);
    
    await suspendServiceWorker();

    let receivedMessageAfter = await chrome.runtime.sendMessage({type: "get-message"});
    chai.assert.equal(receivedMessageAfter, "Uninitialized message");
  });

  it("storage can be used to preserve state", async function() {
    let sentMessage = "This is a message for storage";
    await chrome.runtime.sendMessage({type: "set-message-storage", message: sentMessage});
    let receivedMessageBefore = await chrome.runtime.sendMessage({type: "get-message-storage"});
    chai.assert.equal(receivedMessageBefore, sentMessage);
    
    await suspendServiceWorker();

    let receivedMessageAfter = await chrome.runtime.sendMessage({type: "get-message-storage"});
    chai.assert.equal(receivedMessageAfter, sentMessage);
  });
});


let runner = mocha.run();

runner.on("end", () =>
  sendMessage("end", {failures: runner.failures, total: runner.total})
);

Mocha.reporters.Base.useColors = true;
Mocha.reporters.Base.consoleLog = (...args) => sendMessage("log", args);
new Mocha.reporters.Spec(runner);

window.poll = async function poll() {
  if (messages.length == 0) {
    await new Promise(resolve => {
      emit = resolve;
    });
  }

  let newMessages = messages;
  messages = [];

  return newMessages;
};
