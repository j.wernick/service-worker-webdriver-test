/*
 * This file is part of eyeo's Web Extension Ad Blocking Toolkit (EWE),
 * Copyright (C) 2006-present eyeo GmbH
 *
 * EWE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * EWE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EWE.  If not, see <http://www.gnu.org/licenses/>.
 */

"use strict";

let message = "Uninitialized message";

function onMessage(request, sender, sendResponse) {
  switch (request.type) {
    case "set-message":
      message = request.message;
      sendResponse(message);
      return true;

    case "get-message":
      sendResponse(message);
      return true;

    case "set-message-storage":
      chrome.storage.local.set({ message: request.message }, function() {
        sendResponse(message);
      });
      return true;

    case "get-message-storage":
      chrome.storage.local.get(["message"], function(result) {
        sendResponse(result.message);
      });
      return true;
  }
};


chrome.runtime.onMessage.addListener(onMessage);
chrome.tabs.create({url: "unit.html"});
