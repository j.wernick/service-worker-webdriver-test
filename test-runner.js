/*
 * This file is part of eyeo's Web Extension Ad Blocking Toolkit (EWE),
 * Copyright (C) 2006-present eyeo GmbH
 *
 * EWE is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * EWE is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with EWE.  If not, see <http://www.gnu.org/licenses/>.
 */

/* eslint-env node */

import path from "path";

import webdriver from "selenium-webdriver";
import chrome from "selenium-webdriver/chrome.js";
import "chromedriver";

async function suspendServiceWorker(driver) {
  let currentHandle = await driver.getWindowHandle();
  await driver.switchTo().newWindow("tab");
  await driver.get("chrome://serviceworker-internals/");
  let stopButtons = await driver.findElements(webdriver.By.className("stop"));
  for (let stopButton of stopButtons) {
    if (await stopButton.isDisplayed())
      await stopButton.click();
  }
  await new Promise(r => setTimeout(r, 100));
  await driver.close();
  await driver.switchTo().window(currentHandle);
}

async function pollMessages(driver) {
  let result;
  let log = [];

  while (!result) {
    let messages = await driver.executeAsyncScript(`
      let callback = arguments[0];
      if (document.readyState == "complete")
        callback(window.poll());
      else
        window.addEventListener("load", () => callback(window.poll()));`);

    if (!messages)
      return null;

    for (let [id, arg] of messages) {
      switch (id) {
        case "log":
          log.push(arg);
          console.log(...arg);
          break;

        case "suspendServiceWorker":
          await suspendServiceWorker(driver);
          break;

        case "end":
          result = arg;
          result.log = log;
          break;
      }
    }
  }
  return result;
}

async function run() {
  let extensionDir = path.resolve(process.cwd(), "test-mv3");
  let options = new chrome.Options();
  options.addArguments("no-sandbox", `load-extension=${extensionDir}`);

  let driver = new webdriver.Builder()
      .forBrowser("chrome")
      .setChromeOptions(options)
      .build();

  let resultUnit;
  await new Promise(r => setTimeout(r, 500));
  
  try {
    for (let handle of await driver.getAllWindowHandles()) {
      try {
        await driver.switchTo().window(handle);
        let url = await driver.getCurrentUrl();
        if (url.endsWith("unit.html"))
          break;
      }
      catch (e) {
        continue;
      }
    }
    resultUnit = await pollMessages(driver);
  }
  finally {
    await driver.quit();
  }

  if (resultUnit.failures > 0 || resultUnit.total == 0)
    process.exit(1);
}

run().catch(err => {
  console.error(err instanceof Error ? err.stack : `Error: ${err}`);
  process.exit(1);
});
