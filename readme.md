# Web Extension Manifest V3 Service Worker Suspension Test

This is a minimal example showing an attempt to test behaviour in an
MV3 web extension when a service worker is suspended.

## How to run this test

Clone this repo, then run the following on a terminal:

```
npm install && npm test
```

### Prerequisites

This test requires the following installed:
- NodeJS and NPM
- Chrome 101

If you want to test with a differnent version of Chrome, open
`package.json` and update the version of `chromedriver` to match your
version of Chrome.

## Testing framework infrastucture

This is a simplified version of the testing framework used in
[eyeo's Web Extension Ad Blocking Toolkit](https://gitlab.com/eyeo/adblockplus/abc/webext-sdk).

- There is an unpacked browser extension in ./test-mv3
- This extension has a service worker (./test-mv3/background.js), and
  an extension page (unit.html).
- The extension page, when loaded, runs unit tests using the Mocha
  testing framework.
- If you run `npm intall && npm test`, Selenium Webdriver will be used
  to launch Chrome with the test extension installed and run the test
  (see ./test-runner.js).
- The Webdriver script periodically polls a global variable on the
  test extension page where Mocha is running, which allows the extension page to:
  - Pass log messages back to the Webdriver script.
  - Notify the Webdriver script when it's done.
  - Request that the Webdriver script open
    chrome://serviceworker-internals/ and click the "Stop" button to
    suspend the service worker (the extension page does not have
    permission to open chrome:// urls itself).

## The test

My tests do the following:

1. Send a 'setter' message to set a message in the service worker
   script.
2. Suspend the service worker by clicking the "Stop" button on
   chrome://serviceworker-internals/
3. Send the 'getter' message to get the stored message.

There are two variants on the getter and setter messages. The first
stores a message in memory. The second uses `chrome.storage.local`.

## Expected Behaviour

The service worker should automatically start when the getter message
is sent after it was stopped.

I expect the test that uses in-memory storage to lose its message when
the service worker is suspended, and so responds with its
uninitialized state.

I expect the test hat uses `chrome.storage.local` to return the set
message.

## Observed Behaviour

The service worker does not start after it's been stopped. This causes
the tests to fail from a timeout.
